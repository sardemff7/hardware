# Copyright 2014 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/prismatik/Lightpack}
MY_PNV=${MY_PN}-${PV}

require github [ user=woodenshark pn=${MY_PN} tag=${PV} ] \
        qmake [ slot=5 ] \
        udev-rules [ udev_files=[ dist_linux/deb/etc/udev/rules.d/93-lightpack.rules ] ] \
        freedesktop-desktop \
        gtk-icon-cache

SUMMARY="Prismatik is an open-source software to control Lightpack devices"
DESCRIPTION="
Prismatik is an open-source software build to control Lightpack devices. It grabs the screen,
analyzes the picture, calculates the resulting colors and provides a soft and gentle lighting with
the Lightpack device. It can also handle other devices such as Adalight, Ardulight or even
the Alienware LightFX system.
"
HOMEPAGE+=" http://lightpack.tv"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/systemd [[ note = [ for udev ] ]]
        virtual/usb:1
        x11-dri/mesa [[ note = [ provides libGL ] ]]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/qtbase:5
        x11-libs/qtserialport:5
"

WORK=${WORKBASE}/${MY_PNV}/Software

EQMAKE_SOURCES="${MY_PN}.pro"

src_install() {
    newbin src/bin/Prismatik ${PN}

    insinto /usr/share/applications/
    doins dist_linux/deb/usr/share/applications/${PN}.desktop

    insinto /usr/share/
    doins -r dist_linux/deb/usr/share/{icons,pixmaps}

    install_udev_files

    emagicdocs
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

