# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel project=${PN} tag=igc-${PV} ] \
    cmake [ api=2 ] \
    python [ blacklist=3 multibuild=false ]

SUMMARY="Intel Graphics Compiler"
DESCRIPTION="
The Intel Graphics Compiler for OpenCL is an llvm based compiler for OpenCL targeting Intel Gen
graphics hardware architecture.
"
HOMEPAGE+=" https://01.org/compute-runtime"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/clang:8
        sys-devel/bison
        sys-devel/flex
    build+run:
        dev-lang/llvm:8
        dev-libs/opencl-clang:8
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DIGC_PREFERRED_LLVM_VERSION:STRING=8
    -DLLVM_DIR:PATH=/usr/$(exhost --target)/lib/llvm/8/lib/cmake/llvm
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
)

# https://github.com/intel/intel-graphics-compiler/issues/89
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    cmake_src_prepare

    # fix !!! Exheres bug: 'clang' banned by distribution
    edo sed \
        -e "s:clang-${LLVM_VERSION_MAJOR}:$(exhost --tool-prefix)clang-${LLVM_VERSION_MAJOR}:g" \
        -i "${CMAKE_SOURCE}"/IGC/BiFModule/CMakeLists.txt
}

