# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel tag=v${PV/_p/-} ] \
    cmake [ api=2 ] \
    alternatives

SUMMARY="OpenCL Common Clang"
DESCRIPTION="
Common clang is a thin wrapper library around clang. Common clang has OpenCL-oriented API and is
capable to compile OpenCL C kernels to SPIR-V modules.
"
HOMEPAGE+=" https://01.org/compute-runtime"

LICENCES="UoI-NCSA"
SLOT="8"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-scm/git
    build+run:
        dev-lang/clang:${SLOT}
        dev-lang/llvm:${SLOT}
        dev-util/spirv-llvm-translator:${SLOT}
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLLVM_DIR:PATH=/usr/$(exhost --target)/lib/llvm/${SLOT}/lib/cmake/llvm
    -DLLVMSPIRV_INCLUDED_IN_LLVM:BOOL=FALSE
    -DSPIRV_TRANSLATOR_DIR:PATH=/usr/$(exhost --target)/lib
)

src_install() {
    cmake_src_install

    local arch_dependent_alternatives=(
        /usr/$(exhost --target)/include/cclang{,-${SLOT}}
        /usr/$(exhost --target)/lib/libopencl-clang.so{,-${SLOT}}
    )

    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

