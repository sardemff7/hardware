# Copyright 2016 Jorge Aparicio
# Distributed under the terms of the GNU General Public License v2

SUMMARY="NVIDIA Video Codec SDK"
DESCRIPTION="
The NVIDIA Video Codec SDK is a complete set of high-performance NVIDIA Video
Codecs tools, samples and documentation for hardware encode API (NVENC API) and
hardware decode API (NVCUVID API) on Windows and Linux OS platforms. This SDK
consists of two hardware API interfaces: NVENC for video encode acceleration and
NVCUVID for video decode acceleration with NVIDIA’s Kepler and Maxwell
generations of GPUs.

Use of NVENC API for H.264 encoding, requires Kepler or Maxwell generation GPUs
along with supported driver version.

Use of NVENC API for H.265 (HEVC) encoding requires Maxwell 2nd generation GPUs
along with supported driver version.
"
HOMEPAGE="https://developer.nvidia.com/nvidia-video-codec-sdk"
DOWNLOADS="https://developer.nvidia.com/video-sdk-${PV//./} -> ${PNV}.zip"

LICENCES="NVIDIA-VIDEO-CODEC-SDK"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        x11-drivers/nvidia-drivers[>=358]
"

WORK="${WORKBASE}"/nvidia_video_sdk_${PV}

src_install() {
    insinto /usr/$(exhost --target)/include
    doins Samples/common/inc/nv*.h
}
