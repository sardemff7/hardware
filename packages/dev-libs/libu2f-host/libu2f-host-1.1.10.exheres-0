# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Yubico tag=${PNV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require udev-rules

SUMMARY="Yubico Universal 2nd Factor (U2F) Host C Library"
DESCRIPTION="
Libu2f-host provides a C library and command-line tool that implements the
host-side of the U2F protocol. There are APIs to talk to a U2F device and
perform the U2F Register and U2F Authenticate operations
"
HOMEPAGE="https://developers.yubico.com/libu2f-host"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7"
MYOPTIONS="
    doc
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-devel/gengetopt
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc )
        providers:eudev? ( sys-apps/eudev [[ note = udev.pc ]] )
        providers:systemd? ( sys-apps/systemd [[ note = udev.pc ]] )
    build+run:
        dev-libs/hidapi
        dev-libs/json-c
"

AT_M4DIR=( gl/m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-udevrulesdir="${UDEVRULESDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc gtk-doc'
)

