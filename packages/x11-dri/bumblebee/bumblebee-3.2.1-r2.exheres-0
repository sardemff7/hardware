# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion \
        systemd-service [ systemd_files=[ "${WORK}"/scripts/systemd/bumblebeed.service ] ]

SUMMARY="A project aiming to support NVIDIA Optimus technology under Linux"
HOMEPAGE="http://bumblebee-project.org/"
DOWNLOADS="http://bumblebee-project.org/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-apps/help2man
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        user/bumblebee
        x11-libs/libX11
    run:
        x11-dri/primus
    suggestion:
        x11-dri/bbswitch [[ description = [ power management for nVidia cards ] ]]
        x11-libs/virtualgl [[ description = [ an alternative rendering bridge to primus ] ]]
"

BUGS_TO="mixi@exherbo.org"

src_prepare() {
    # Upstream: http://github.com/Bumblebee-Project/Bumblebee/commit/82442975db743e79f2676fae20a0d7d6ef710307
    edo sed '/CPUSchedulingPolicy=idle/d' -i scripts/systemd/bumblebeed.service.in
    # If users don't run a display manager the service won't start
    edo sed 's/graphical/multi-user/' -i scripts/systemd/bumblebeed.service.in
}

src_configure() {
    CONF_BRIDGE=primus \
    CONF_DRIVER=nvidia \
    CONF_DRIVER_MODULE_NVIDIA=nvidia \
    CONF_LDPATH_NVIDIA=/etc/env.d/alternatives/opengl/nvidia-drivers/usr/$(exhost --target)/lib \
    CONF_MODPATH_NVIDIA=/etc/env.d/alternatives/opengl/nvidia-drivers/usr/$(exhost --target)/lib/xorg/modules,/usr/$(exhost --target)/lib/xorg/modules \
    CONF_SOCKPATH=/run/bumblebee.sock \
        econf \
            --without-pidfile \
            --with-udev-rules=/usr/$(exhost --target)/lib/udev/rules.d
}

src_compile() {
    default

    emake scripts/systemd/bumblebeed.service
}

src_install() {
    default

    # Properly install the bash-completion stuff.
    dobashcompletion "${IMAGE}"/etc/bash_completion.d/bumblebee
    edo rm -r "${IMAGE}"/etc/bash_completion.d

    insinto "${SYSTEMDSYSTEMUNITDIR}"
    doins scripts/systemd/bumblebeed.service
}

pkg_postinst() {
    default

    local cruft=(
        "${ROOT}"/etc/bash_completion.d/bumblebee
    )
    for file in "${cruft[@]}"; do
        if [[ -f ${file} || -L ${file} ]]; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

