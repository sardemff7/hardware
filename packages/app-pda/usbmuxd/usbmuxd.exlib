# Copyright 2009 Ingmar Vanhassel
# Copyright 2010-2014 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.sukimashita.com/${PN}.git"
    require scm-git
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
else
    DOWNLOADS="https://www.libimobiledevice.org/downloads/${PNV}.tar.bz2"
fi

require udev-rules systemd-service

SUMMARY="USB Multiplex Daemon: Talk to your iDevice (e. g. iPhone) over USB"
DESCRIPTION="
USB Multiplex Daemon. This bit of software is in charge of talking to your iDevice
(e. g. iPhone, iPod Touch, iPad, etc.) over USB and coordinating access to its
services by other applications.
"
HOMEPAGE="https://cgit.sukimashita.com/${PN}.git/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-pda/libimobiledevice[>=1.1.6]
        app-pda/libusbmuxd[>=1.0.10]
        dev-libs/libplist[>=1.12]
        sys-apps/systemd
        virtual/usb:1
        group/plugdev
        group/usb
        user/usbmux
"

if ever at_least scm; then
    DEPENDENCIES+="
        build+run:
            app-pda/libimobiledevice[>=1.2.1]
    "
fi

BUGS_TO="philantrop@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --sbindir=/usr/$(exhost --target)/bin
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-udevrulesdir="${UDEVRULESDIR}"
)

